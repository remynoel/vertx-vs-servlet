Test de performance entre Vert.x & Tomcat Embedded

wrk -t4 -c500 -d30s http://127.0.0.1:8080/  
wrk -t4 -c1000 -d30s http://127.0.0.1:8080/  
wrk -t4 -c2000 -d30s http://127.0.0.1:8080/