package com.bench;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.ext.web.Router;

public class VertxBench {
    public static void main(String[] args) {
        VertxOptions options = new VertxOptions();
        Vertx vertx = Vertx.vertx(options);
        Router router = Router.router(vertx);

        router.get("/").handler(routingContext -> {
            vertx.setTimer(100, (aVoid) -> {
                routingContext.response().end("Hello world");
            });
        });
        vertx.createHttpServer().requestHandler(router::accept).listen(8080);
    }
}
